### Docker Image ###
<ol>
    <li>**Prerequisite**: Git, Docker installed</li>
    <li>git clone the repository</li>
    <li>cd exercise</li>
    <li>sh docker-build.sh</li>
    <li>After few seconds, Browser connected: http://127.0.0.1:8080/ will be shown</li>
</ol>

### Branching strategy ###
Branches like development, feature, release, hotfix allow every member complete the task in isolation and track changes directly to the initial request.

### Release strategy ###
Create a new test for each new function, utilize continous intergration to automatically run the test in order to integrate to branches and continous deployment pipelines in order to deploy the code and monitor over the process.

### The rationale of pipeline design ###
<ol>
    <li>
        **Build**: Combine the source code and its dependencies to build a runnable instance
    </li>
    <li>
        **Test**: Run automated test to ensure the instance is working correctly
    </li>
    <li>
        **Security Scanning**: Check the source code for known vulnerabilities (Gitlab SAST)
    </li>
    <li>
        **Deploy**: Ready to deploy the instance on different environments
    </li>
</ol>

### Handle credentials ###
.env, Jenkins credential() helper method

### Security standard fo the credentials creation ###
At least 15 characters long includes number, lowercase & uppercase letter and character.