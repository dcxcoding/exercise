FROM python:3.10.0-alpine3.14
WORKDIR /usr/src/app
COPY . .
EXPOSE 8080
RUN apk update
RUN apk upgrade
RUN pip install mkdocs
RUN mkdocs build
RUN mkdocs serve